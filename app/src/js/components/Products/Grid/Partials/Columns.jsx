import React from 'react';

const Columns = ({ actions }) => (
	<thead>
		<tr>
			<th>Nombre de Producto</th>
			<th>Area</th>
			<th>Parametro</th>
			<th>Resultado</th>
			<th>Limite Minimo</th>
			<th>Limite Maximo</th>
			<th>Metodo</th>
			{actions && (<th>Acciones</th>)}
		</tr>
	</thead>
);

export default Columns;
