import React from 'react';

const Columns = () => (
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Apellido</th>
			<th>Cedula</th>
			<th>Telefono</th>
			<th>Email</th>
			<th>Acciones</th>
		</tr>
	</thead>
);

export default Columns;
