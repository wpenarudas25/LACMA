import React from 'react';

import Input from '../../../Common/Input';

const Fields = (props) => (
	<div>
		<Input
			id="company"
			name="company"
			text="Empresa Solicitante"
			autoFocus={true}
			icon="closed_caption"
			classNameIcon="prefix"
			onChange={props.loadClient}
		/>
	
		<Input
			id="nit"
			name="nit"
			text="Nit"
			icon="fingerprint"
			classNameIcon="prefix"
			onChange={props.loadClient}
		/>
		<Input
			id="contact"
			name="contact"
			text="Contacto"
			icon="perm_contact_calendar"
			classNameIcon="prefix"
			onChange={props.loadClient}
		/>
		<Input
			id="numContac"
			name="numContac"
			text="Telefono"
			icon="contact_phone"
			classNameIcon="prefix"
			onChange={props.loadClient}
		/>
		<Input
			id="extension"
			type="number"
			name="extension"
			text="Extensión"
			icon="local_phone"
			classNameIcon="prefix"
			onChange={props.loadClient}
		/>
		<Input
			id="email"
			name="email"
			text="E-mail"
			icon="email"
			classNameIcon="prefix"
			onChange={props.loadClient}
		/>
		
	</div>
);

export default Fields;
