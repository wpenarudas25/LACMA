import React from 'react';

const Columns = () => (
	<thead>
		<tr>
			<th>Empresa Solicitante</th>
			<th>Nit</th>
			<th>Contacto</th>
			<th>Email</th>
			<th>Acciones</th>
		</tr>
	</thead>
);

export default Columns;
